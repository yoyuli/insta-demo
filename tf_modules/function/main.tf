variable "function_bucket" {

}

variable "function_name" {

}
variable "entry_point" {
  default = "run"
}


variable "BITBUCKET_COMMIT" {
}

variable "ENV" {

}

variable "env_vars" {
  
}



resource "google_storage_bucket_object" "code_archive" {
  name   = "functions/${var.function_name}_${var.BITBUCKET_COMMIT}.zip"
  bucket = "${var.function_bucket}"
  source = "../.tmp/${var.function_name}.zip"
}

resource "google_cloudfunctions_function" "function" {
  name        = "${var.function_name}_${var.ENV}"
  description = "Cloudfunction for ${var.function_name}"
  runtime     = "python37"

  available_memory_mb = 128
  trigger_http        = true
  entry_point         = "${var.entry_point}"

  source_archive_bucket = "${var.function_bucket}"
  source_archive_object = "${google_storage_bucket_object.code_archive.name}"

  environment_variables = "${var.env_vars}"
}