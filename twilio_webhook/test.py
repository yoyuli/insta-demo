import unittest
from unittest.mock import MagicMock

class TwilioRequestProcessing(unittest.TestCase):
    def test_receive_text_message(self):
        print("\nTesting receiving text message")
        from twilio_request_module import Twilio_Request
        request=MagicMock()
        request.content_type='mock'
        request.form={
            'SmsMessageSid': 'SMb5348e053762aef7945cd6d3376fc7df',
            'NumMedia': '0',
            'SmsSid': 'SMb5348e053762aef7945cd6d3376fc7df', 
            'SmsStatus': 'received',
            'Body': 'Hi', 
            'To': 'whatsapp:+14155238886', 
            'NumSegments': '1', 
            'MessageSid': 'SMb5348e053762aef7945cd6d3376fc7df', 
            'AccountSid': 'AC099efff72f3177f93d2e1b5cbcb078b6', 
            'From': 'whatsapp:+447853972478', 
            'ApiVersion': '2010-04-01'
        }
        self.assertEqual(Twilio_Request(request).text,'Hi',"Should say 'Hi'")

    def test_receive_media_message(self):
        print("\nTesting receiving media message")
        from twilio_request_module import Twilio_Request
        request=MagicMock()
        request.content_type='mock'
        request.form={
            'SmsMessageSid': 'SMb5348e053762aef7945cd6d3376fc7df',
            'NumMedia': '1',
            'SmsSid': 'SMb5348e053762aef7945cd6d3376fc7df', 
            'SmsStatus': 'received',
            'Body': 'Hi', 
            'To': 'whatsapp:+14155238886', 
            'NumSegments': '1', 
            'MessageSid': 'SMb5348e053762aef7945cd6d3376fc7df', 
            'AccountSid': 'AC099efff72f3177f93d2e1b5cbcb078b6', 
            'From': 'whatsapp:+447853972478', 
            'ApiVersion': '2010-04-01',
            'MediaContentType0': 'image/jpeg',
            'MediaUrl0': 'https://api.twilio.com/2010-04-01/Accounts/AC099efff72f3177f93d2e1b5cbcb078b6/Messages/MM3648977bdca64a5fefffbe8107db4639/Media/ME423a21a8ace76ab06f3cf97b33730596'
        }
        self.assertEqual(Twilio_Request(request).media,
            'https://api.twilio.com/2010-04-01/Accounts/AC099efff72f3177f93d2e1b5cbcb078b6/Messages/MM3648977bdca64a5fefffbe8107db4639/Media/ME423a21a8ace76ab06f3cf97b33730596',
            "Should have media URL")
    
    def test_receive_unsupported_media(self):
        print("\nTesting receiving unsupported media message")
        from twilio_request_module import Twilio_Request
        request=MagicMock()
        request.content_type='mock'
        request.form={
            'SmsMessageSid': 'SMb5348e053762aef7945cd6d3376fc7df',
            'NumMedia': '1',
            'SmsSid': 'SMb5348e053762aef7945cd6d3376fc7df', 
            'SmsStatus': 'received',
            'Body': 'Hi', 
            'To': 'whatsapp:+14155238886', 
            'NumSegments': '1', 
            'MessageSid': 'SMb5348e053762aef7945cd6d3376fc7df', 
            'AccountSid': 'AC099efff72f3177f93d2e1b5cbcb078b6', 
            'From': 'whatsapp:+447853972478', 
            'ApiVersion': '2010-04-01',
            'MediaContentType0': 'image/png',
            'MediaUrl0': 'https://api.twilio.com/2010-04-01/Accounts/AC099efff72f3177f93d2e1b5cbcb078b6/Messages/MM3648977bdca64a5fefffbe8107db4639/Media/ME423a21a8ace76ab06f3cf97b33730596'
        }
        self.assertFalse(hasattr(Twilio_Request(request),'media'),
            "Should not have media URL")
        
if __name__ == '__main__':
    unittest.main()