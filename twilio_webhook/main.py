import os   
env = os.environ 

import logging
logger=logging.getLogger(__name__)
logger.setLevel(env.get("LOG_LEVEL") if env.get("LOG_LEVEL") else "DEBUG")
logger.debug("Function name: %s Commit version: %s" % (env.get("FUNCTION_NAME"),env.get("BITBUCKET_COMMIT")))


from twilio_request_module import Twilio_Request

def run(request):
    logger.debug(request.content_type)
    
    twilio_request=Twilio_Request(request)
    

    return
