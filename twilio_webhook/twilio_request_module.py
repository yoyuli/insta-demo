import os   
env = os.environ 

import logging

logger=logging.getLogger(__name__)
logger.setLevel(env.get("LOG_LEVEL") if env.get("LOG_LEVEL") else "DEBUG")

from twilio.rest import Client

account_sid = env.get("TWILIO_ID")
auth_token = env.get("TWILIO_TOKEN")
client = Client(account_sid, auth_token)

SANDBOX='whatsapp:+14155238886'

import emoji

class Twilio_Request:
    def __init__(self,request):
        # object attributes:
        # - event_type (e.g.'received','delivered')
        # - sender (a mobile number)
        # - text
        # - media (the URL of the jpeg media)
        
        try:
            logger.debug(request.form)
        except:
            logger.debug("error printing form")
        
        self.event_type=request.form['SmsStatus']
        if self.event_type=='received':
            self.sender=request.form['From'].split(':')[1]
            self.text=request.form['Body']
            # self.reply('Hi There :blush:')

            NumMedia=int(request.form['NumMedia'])
            if NumMedia> 0 :
                if NumMedia==1 and request.form['MediaContentType0']=='image/jpeg':
                    self.media=request.form['MediaUrl0']
                else:
                    self.reply('Sorry, we cannot process the media type you sent to us yet. :confounded:')
            
        else:
            pass

        return 

    def reply(self,message):
        message = client.messages.create(
                            body=emoji.emojize(message,use_aliases=True),
                            from_=SANDBOX,
                            to='whatsapp:%s' % self.sender # 'whatsapp:' seems to be required when sending from sandbox 
                        )
        # TODO: Media can be attached using `media_url`

        logger.info("Message ID: %s" % message.sid)