terraform {
  backend "gcs" {}
}

provider "google" {
  project = "${var.PROJECT}"
  region  = "${var.REGION}"
  zone    = "${var.ZONE}"
}
resource "google_project_service" "dialogflow_api" {
  project = "${var.PROJECT}"
  service = "dialogflow.googleapis.com"
  disable_dependent_services = true
}