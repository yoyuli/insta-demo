module "twilio_webhook" {
  source = "../tf_modules/function"
  function_name = "twilio_webhook"
  function_bucket = "${local.artefacts_bucket_name}"
  ENV = "${var.ENV}"
  BITBUCKET_COMMIT = "${var.BITBUCKET_COMMIT}"
  env_vars={
    BITBUCKET_COMMIT = "${var.BITBUCKET_COMMIT}",
    TWILIO_ID = "${var.TWILIO_ID}"
    TWILIO_TOKEN = "${var.TWILIO_TOKEN}"
  }
}
