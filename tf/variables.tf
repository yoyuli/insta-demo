variable "PROJECT" {}

variable "ENV" {}

variable "REGION" {
  default = "europe-west2"
}

variable "ZONE" {
  default = "europe-west2-b"
}

variable "BITBUCKET_COMMIT" {

}

variable "TWILIO_ID" {
  
}

variable "TWILIO_TOKEN" {
  
}

locals {
  artefacts_bucket_name = "artefacts-${var.PROJECT}-${var.ENV}"
}